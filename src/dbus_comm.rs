use dbus::arg::*;
use dbus::blocking::Connection;
use dbus::message::MatchRule;
use dbus::strings::Path;
use dbus::Message;

use crate::dbus_bluez::*;
use crate::dbus_bluez_char::OrgBluezGattCharacteristic1;
use crate::dbus_bluez_device;
use crate::dbus_bluez_device::OrgBluezDevice1;
use crate::dbus_bluez_hci::OrgBluezAdapter1;

use crossbeam::channel::{Receiver, Sender, TryRecvError};

use std::collections::HashMap;
use std::thread;
use std::time::Duration;

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Device {
  Normal,
  Dfu,
}

#[derive(Debug)]
pub enum Event {
  Disconnected,
  Connected,
  Added,
  Removed,
  ServicesResolved,
  ServicesUnresolved,
  NotificationsEnabled,
  NotificationsDisabled,
  ValueReceived(Vec<u8>),
  DBusError(dbus::Error),
  Timeout,
}

#[derive(Debug, PartialEq)]
#[allow(dead_code)]
pub enum Command {
  ScanStart,
  ScanStop,
  Connect(Device),
  Disconnect(Device),
  EnableNotifications(Device, String),
  DisableNotifications(Device, String),
  WriteValue(Device, String, Vec<u8>),
}

#[derive(Debug)]
pub struct DBusEvent {
  pub dev: Option<Device>,
  pub evt: Event,
}

#[derive(Debug, PartialEq)]
pub struct DBusCommand {
  pub cmd: Command,
}

unsafe impl Send for DBusEvent {}
unsafe impl Sync for DBusEvent {}
unsafe impl Send for DBusCommand {}
unsafe impl Sync for DBusCommand {}

#[derive(Debug)]
pub struct CharacteristicData {
  pub uuid: String,
  pub characteristic_path: String,
}

#[derive(Debug)]
pub struct ServiceData {
  pub service_path: String,
  pub characteristics: Vec<CharacteristicData>,
}

#[derive(Debug, Clone)]
pub struct DBusCommunicator {
  hci_path: Option<String>,
  dev_path: Option<String>,
  dfu_dev_path: Option<String>,
  mac_addr: String,
  dfu_mac_addr: String,
}

impl DBusCommunicator {
  pub fn new(event_s: Sender<DBusEvent>, cmd_r: Receiver<DBusCommand>, mac_addr: &str, dfu_mac_addr: &str) -> Self {
    let mut dbus_comm = DBusCommunicator {
      hci_path: None,
      dev_path: None,
      dfu_dev_path: None,
      mac_addr: mac_addr.into(),
      dfu_mac_addr: dfu_mac_addr.into(),
    };

    if let Some(path) = dbus_comm.get_adapter_path() {
      log::debug!("HCI Path: {}", path);
      dbus_comm.hci_path = Some(path);

      let path = dbus_comm.hci_path.clone().unwrap();
      dbus_comm.dev_path = Some(
        Path::new(format!("{}{}", path, convert_mac_addr_to_device_path(mac_addr)))
          .unwrap()
          .to_string(),
      );

      log::debug!("Dev path: {:?}", dbus_comm.dev_path.as_ref().unwrap());

      dbus_comm.dfu_dev_path = Some(
        Path::new(format!("{}{}", path, convert_mac_addr_to_device_path(dfu_mac_addr)))
          .unwrap()
          .to_string(),
      );

      log::debug!("DFU Dev path: {:?}", dbus_comm.dfu_dev_path.as_ref().unwrap());
    } else {
      panic!("Couldn't find adapter");
    }

    dbus_comm.start_processing_thread(event_s, cmd_r);

    dbus_comm
  }

  pub fn is_device_cached(&self, dev: Device) -> bool {
    let mut ret = false;

    let conn = Connection::new_system().unwrap();
    let adapter_proxy = conn.with_proxy("org.bluez", "/", Duration::from_secs(10));

    let objs = adapter_proxy.get_managed_objects().unwrap();

    let path;

    match dev {
      Device::Normal => {
        path = self.dev_path.as_ref().unwrap();
      }
      Device::Dfu => {
        path = self.dfu_dev_path.as_ref().unwrap();
      }
    }

    for obj in objs {
      if obj.0.contains(path) {
        ret = true;
        break;
      }
    }

    ret
  }

  pub fn is_scanning(&self) -> bool {
    let conn = Connection::new_system().unwrap();
    let hci_proxy = conn.with_proxy("org.bluez", self.hci_path.as_ref().unwrap(), Duration::from_secs(10));

    hci_proxy.discovering().unwrap()
  }

  pub fn get_services(&self, dev: Device) -> HashMap<String, ServiceData> {
    let conn = Connection::new_system().unwrap();
    let adapter_proxy = conn.with_proxy("org.bluez", "/", Duration::from_secs(10));

    let objs = adapter_proxy.get_managed_objects().unwrap();

    let mut characteristics = vec![];
    let mut services = vec![];

    let path;
    match dev {
      Device::Normal => {
        path = self.dev_path.as_ref().unwrap();
      }
      Device::Dfu => {
        path = self.dfu_dev_path.as_ref().unwrap();
      }
    }

    // grab all of the characteristics and services found for device
    for obj in objs {
      if obj.0.contains(path) {
        if let Some(item) = obj.1.get("org.bluez.GattCharacteristic1") {
          let characteristic_path = obj.0.to_string();
          let service_path = item.get("Service").unwrap().as_str().unwrap().to_string();
          let uuid = item.get("UUID").unwrap().as_str().unwrap().to_string();

          characteristics.push((uuid, service_path, characteristic_path));
        }

        if let Some(item) = obj.1.get("org.bluez.GattService1") {
          let service_path = obj.0.to_string();
          let uuid = item.get("UUID").unwrap().as_str().unwrap().to_string();

          services.push((uuid, service_path));
        }

        // if let Some(item) = obj.1.get("org.bluez.GattDescriptor1") {
        //     let descriptor_path = obj.0.to_string();
        // }
      }
    }

    let mut service_tree = HashMap::new();

    // create a tree where each service is mapped to a vector of all of its characteristics
    for service in services {
      let mut c = vec![];
      for characteristic in &characteristics {
        if characteristic.1 == service.1 {
          c.push(CharacteristicData {
            uuid: characteristic.0.clone(),
            characteristic_path: characteristic.2.clone(),
          });
        }
      }

      service_tree.insert(
        service.0,
        ServiceData {
          service_path: service.1,
          characteristics: c,
        },
      );
    }

    log::trace!("Service Tree: {:?}", service_tree);

    service_tree
  }

  pub fn is_connected(&self, dev: Device) -> bool {
    let conn = Connection::new_system().unwrap();
    let path;

    match dev {
      Device::Normal => {
        path = self.dev_path.as_ref().unwrap();
      }
      Device::Dfu => {
        path = self.dfu_dev_path.as_ref().unwrap();
      }
    }

    let dev_proxy = conn.with_proxy("org.bluez", path, Duration::from_secs(30));

    dev_proxy.connected().unwrap()
  }

  pub fn are_services_resolved(&self, dev: Device) -> bool {
    let conn = Connection::new_system().unwrap();
    let path;

    match dev {
      Device::Normal => {
        path = self.dev_path.as_ref().unwrap();
      }
      Device::Dfu => {
        path = self.dfu_dev_path.as_ref().unwrap();
      }
    }

    let dev_proxy = conn.with_proxy("org.bluez", path, Duration::from_secs(30));

    dev_proxy.services_resolved().unwrap()
  }

  pub fn is_notifying(&self, char: &str) -> bool {
    let conn = Connection::new_system().unwrap();

    let char_proxy = conn.with_proxy("org.bluez", char, Duration::from_secs(5));

    char_proxy.notifying().unwrap()
  }

  fn get_adapter_path(&self) -> Option<String> {
    let conn = Connection::new_system().unwrap();
    let adapter_proxy = conn.with_proxy("org.bluez", "/", Duration::from_secs(10));

    let objs = adapter_proxy.get_managed_objects().unwrap();

    for obj in objs {
      // obj.0 is the path
      // obj.1 is the hashmap of adapter data
      if let Some(_) = obj.1.get("org.bluez.Adapter1") {
        return Some(obj.0.to_string());
      }
    }

    None
  }

  fn start_processing_thread(&self, event_s: Sender<DBusEvent>, cmd_r: Receiver<DBusCommand>) {
    let conn = Connection::new_system().unwrap();
    let path = self.hci_path.clone().unwrap();
    let dev_path = self.dev_path.clone().unwrap();
    let dfu_dev_path = self.dfu_dev_path.clone().unwrap();
    let sync_event_s = event_s.clone();

    // when a scan is initiated and the device is detected, the signal will be caught here
    self.add_interfaces_added_callback(&conn, event_s.clone(), self.mac_addr.clone(), self.dfu_mac_addr.clone());
    // when the device was once detected but is no longer detected, the signal will be caught here
    self.add_interfaces_removed_callback(&conn, event_s.clone(), dev_path.clone(), dfu_dev_path.clone());
    // any signals outside of the initial detection and detection loss will be caught here
    // i.e. RSSI strength changes, notification values, connected/disconnected events, etc
    self.add_properties_changed_callback(&conn, event_s.clone(), dev_path.clone(), dfu_dev_path.clone());

    // thread that handles commands which are sent from the controller thread.
    // events that are received from DBus are handled in 10ms intervals during the con.process call
    thread::spawn(move || {
      // proxy that deals with hci adapter commands
      let hci_proxy = conn.with_proxy("org.bluez", path, Duration::from_secs(10));
      // proxy that deals with normal device commands
      let dev_proxy = conn.with_proxy("org.bluez", &dev_path, Duration::from_secs(20));
      // proxy that deals with dfu device commands
      let dfu_dev_proxy = conn.with_proxy("org.bluez", &dfu_dev_path, Duration::from_secs(20));

      loop {
        conn.process(Duration::from_millis(10)).unwrap();

        match cmd_r.try_recv() {
          Ok(x) => match x.cmd {
            Command::ScanStart => {
              log::trace!("Starting scan");
              match hci_proxy.start_discovery() {
                Ok(_) => {}
                Err(e) => {
                  sync_event_s
                    .send(DBusEvent {
                      dev: None,
                      evt: Event::DBusError(e),
                    })
                    .unwrap();
                }
              }
            }
            Command::ScanStop => {
              log::trace!("Stopping scan");
              match hci_proxy.stop_discovery() {
                Ok(_) => {}
                Err(e) => {
                  sync_event_s
                    .send(DBusEvent {
                      dev: None,
                      evt: Event::DBusError(e),
                    })
                    .unwrap();
                }
              }
            }
            Command::Connect(dev) => {
              let proxy;
              match dev {
                Device::Normal => {
                  log::trace!("Connecting to normal device");
                  proxy = &dev_proxy;
                }
                Device::Dfu => {
                  log::trace!("Connecting to dfu device");
                  proxy = &dfu_dev_proxy;
                }
              }

              match proxy.connect() {
                Ok(_) => {}
                Err(e) => {
                  sync_event_s
                    .send(DBusEvent {
                      dev: Some(dev),
                      evt: Event::DBusError(e),
                    })
                    .unwrap();
                }
              }
            }
            Command::Disconnect(dev) => {
              let proxy;
              match dev {
                Device::Normal => {
                  log::trace!("Disconnecting from normal device");
                  proxy = &dev_proxy;
                }
                Device::Dfu => {
                  log::trace!("Disconnecting from dfu device");
                  proxy = &dfu_dev_proxy;
                }
              }

              match proxy.disconnect() {
                Ok(_) => {}
                Err(e) => {
                  sync_event_s
                    .send(DBusEvent {
                      dev: Some(dev),
                      evt: Event::DBusError(e),
                    })
                    .unwrap();
                }
              }
            }
            Command::EnableNotifications(dev, char_path) => {
              match dev {
                Device::Normal => {
                  log::trace!("Enabling notifications for normal device on {:?}", char_path);
                }
                Device::Dfu => {
                  log::trace!("Enabling notifications for dfu device on {:?}", char_path);
                }
              }

              let char_proxy = conn.with_proxy("org.bluez", char_path, Duration::from_secs(5));
              match char_proxy.start_notify() {
                Ok(_) => {}
                Err(e) => {
                  sync_event_s
                    .send(DBusEvent {
                      dev: Some(dev),
                      evt: Event::DBusError(e),
                    })
                    .unwrap();
                }
              }
            }
            Command::DisableNotifications(dev, char_path) => {
              match dev {
                Device::Normal => {
                  log::trace!("Disabling notifications for normal device on {:?}", char_path);
                }
                Device::Dfu => {
                  log::trace!("Disabling notifications for dfu device on {:?}", char_path);
                }
              }

              let char_proxy = conn.with_proxy("org.bluez", char_path, Duration::from_secs(5));
              match char_proxy.stop_notify() {
                Ok(_) => {}
                Err(e) => {
                  sync_event_s
                    .send(DBusEvent {
                      dev: Some(dev),
                      evt: Event::DBusError(e),
                    })
                    .unwrap();
                }
              }
            }
            Command::WriteValue(dev, char_path, vals) => {
              match dev {
                Device::Normal => {
                  log::trace!("Writing {:?} to normal device on {:?}", vals, char_path);
                }
                Device::Dfu => {
                  log::trace!("Writing {:?} to dfu device on {:?}", vals, char_path);
                }
              }

              let char_proxy = conn.with_proxy("org.bluez", char_path, Duration::from_secs(5));

              let mut value_arg = HashMap::new();
              value_arg.insert(
                "offset".to_string(),
                Variant(Box::new(0 as u16) as Box<dyn RefArg + 'static>),
              );
              match char_proxy.write_value(vals, value_arg) {
                Ok(_) => {}
                Err(e) => {
                  sync_event_s
                    .send(DBusEvent {
                      dev: Some(dev),
                      evt: Event::DBusError(e),
                    })
                    .unwrap();
                }
              }
            }
          },
          Err(e) => match e {
            TryRecvError::Empty => (),
            TryRecvError::Disconnected => {
              log::error!("Main thread killed. Exiting");
              std::process::exit(1);
            }
          },
        }
      }
    });
  }

  fn add_interfaces_added_callback(
    &self,
    conn: &Connection,
    event_s: Sender<DBusEvent>,
    mac_addr: String,
    dfu_mac_addr: String,
  ) {
    let interfaces_added_rule = MatchRule::new_signal("org.freedesktop.DBus.ObjectManager", "InterfacesAdded");
    let _adv_match_token = conn
      .add_match(
        interfaces_added_rule,
        move |_o: OrgFreedesktopDBusObjectManagerInterfacesAdded, _: &Connection, msg: &Message| {
          let (_path, parsed_args): (Path, HashMap<String, HashMap<String, Variant<Box<dyn RefArg>>>>) =
            msg.read_all().unwrap();

          if let Some(device) = parsed_args.get("org.bluez.Device1") {
            if let Some(addr) = device.get("Address") {
              // normal device was found in scan
              if addr.as_str().unwrap() == mac_addr {
                event_s
                  .send(DBusEvent {
                    dev: Some(Device::Normal),
                    evt: Event::Added,
                  })
                  .unwrap();
              // dfu device was found in scan
              } else if addr.as_str().unwrap() == dfu_mac_addr {
                event_s
                  .send(DBusEvent {
                    dev: Some(Device::Dfu),
                    evt: Event::Added,
                  })
                  .unwrap();
              }
            }
          }

          true
        },
      )
      .unwrap();
  }

  fn add_interfaces_removed_callback(
    &self,
    conn: &Connection,
    event_s: Sender<DBusEvent>,
    dev_path: String,
    dfu_dev_path: String,
  ) {
    let interfaces_removed_rule = MatchRule::new_signal("org.freedesktop.DBus.ObjectManager", "InterfacesRemoved");
    let _adv_match_token = conn
      .add_match(
        interfaces_removed_rule,
        move |_o: OrgFreedesktopDBusObjectManagerInterfacesRemoved, _: &Connection, msg: &Message| {
          let (path, _parsed_args): (Path, Vec<String>) = msg.read_all().unwrap();

          // normal device was removed from cache
          if dev_path == path.to_string() {
            event_s
              .send(DBusEvent {
                dev: Some(Device::Normal),
                evt: Event::Removed,
              })
              .unwrap();
          // dfu device was removed from cache
          } else if dfu_dev_path == path.to_string() {
            event_s
              .send(DBusEvent {
                dev: Some(Device::Dfu),
                evt: Event::Removed,
              })
              .unwrap();
          }

          true
        },
      )
      .unwrap();
  }

  fn add_properties_changed_callback(
    &self,
    conn: &Connection,
    event_s: Sender<DBusEvent>,
    dev_path: String,
    dfu_dev_path: String,
  ) {
    let prop_changed_rule = MatchRule::new_signal("org.freedesktop.DBus.Properties", "PropertiesChanged");
    let _conn_match_token = conn
      .add_match(
        prop_changed_rule,
        move |_o: dbus_bluez_device::OrgFreedesktopDBusPropertiesPropertiesChanged, _: &Connection, msg: &Message| {
          let (_, parsed_args): (String, HashMap<String, Variant<Box<dyn RefArg>>>) = msg.read_all().unwrap();

          let path = msg.path().unwrap().to_string();

          let mut dev_type = None;
          if path.contains(&dev_path) {
            dev_type = Some(Device::Normal);
          } else if path.contains(&dfu_dev_path) {
            dev_type = Some(Device::Dfu);
          }

          if dev_type != None {
            // response value received
            if let Some(_vals) = parsed_args.get("Value") {
              let vals = parse_write_response(&parsed_args);
              let _service_path = parse_service_path(&path);

              event_s
                .send(DBusEvent {
                  dev: dev_type,
                  evt: Event::ValueReceived(vals),
                })
                .unwrap();

            // notification response received
            } else if let Some(notifying) = parsed_args.get("Notifying") {
              let _service_path = parse_service_path(&path);

              if notifying.as_i64().unwrap() == 1 {
                event_s
                  .send(DBusEvent {
                    dev: dev_type,
                    evt: Event::NotificationsEnabled,
                  })
                  .unwrap();
              } else {
                event_s
                  .send(DBusEvent {
                    dev: dev_type,
                    evt: Event::NotificationsDisabled,
                  })
                  .unwrap();
              }
            // either connection or services resolved response received
            } else {
              // Connected and ServicesResolved events can come in the same packet
              // and therefore must both be checked for each packet
              if let Some(conn_status) = parsed_args.get("Connected") {
                if conn_status.as_i64().unwrap() == 1 {
                  event_s
                    .send(DBusEvent {
                      dev: dev_type,
                      evt: Event::Connected,
                    })
                    .unwrap();
                } else {
                  event_s
                    .send(DBusEvent {
                      dev: dev_type,
                      evt: Event::Disconnected,
                    })
                    .unwrap();
                }
              }

              if let Some(resolved) = parsed_args.get("ServicesResolved") {
                if resolved.as_i64().unwrap() == 1 {
                  event_s
                    .send(DBusEvent {
                      dev: dev_type,
                      evt: Event::ServicesResolved,
                    })
                    .unwrap();
                } else {
                  event_s
                    .send(DBusEvent {
                      dev: dev_type,
                      evt: Event::ServicesUnresolved,
                    })
                    .unwrap();
                }
              }
            }
          }

          true
        },
      )
      .unwrap();
  }
}

fn convert_mac_addr_to_device_path(mac_addr: &str) -> String {
  let new_mac_addr = mac_addr.replace(":", "_");
  format!("{}{}", "/dev_", new_mac_addr)
}

fn parse_service_path(path: &str) -> String {
  let base: Vec<&str> = path.split('/').collect();
  let base_path = base.get(1..6).unwrap();
  [
    "/",
    base_path[0],
    "/",
    base_path[1],
    "/",
    base_path[2],
    "/",
    base_path[3],
    "/",
    base_path[4],
  ]
  .concat()
}

fn parse_write_response(packet: &HashMap<String, Variant<Box<dyn RefArg>>>) -> Vec<u8> {
  let mut res: Vec<u8> = Vec::new();
  packet
    .get(&"Value".to_owned())
    .map_or_else(|| panic!("No values"), |n| n.as_iter().expect("Received 'None' value"))
    .filter_map(|n| n.as_iter())
    .flatten()
    .filter_map(|n| n.as_i64())
    .for_each(|n| res.push(n as u8));

  res
}
