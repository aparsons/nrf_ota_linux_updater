// This code was autogenerated with `dbus-codegen-rust -s -g -d org.bluez -c blocking -p /org/bluez/hci0/dev_EB_BA_71_30_C4_B2/service000e/char000f -o src/dbus_bluez_char.rs`, see https://github.com/diwic/dbus-rs
use dbus as dbus;
#[allow(unused_imports)]
use dbus::arg;
use dbus::blocking;

pub trait OrgFreedesktopDBusIntrospectable {
    fn introspect(&self) -> Result<String, dbus::Error>;
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgFreedesktopDBusIntrospectable for blocking::Proxy<'a, C> {

    fn introspect(&self) -> Result<String, dbus::Error> {
        self.method_call("org.freedesktop.DBus.Introspectable", "Introspect", ())
            .and_then(|r: (String, )| Ok(r.0, ))
    }
}

pub trait OrgBluezGattCharacteristic1 {
    fn read_value(&self, options: arg::PropMap) -> Result<Vec<u8>, dbus::Error>;
    fn write_value(&self, value: Vec<u8>, options: arg::PropMap) -> Result<(), dbus::Error>;
    fn acquire_write(&self, options: arg::PropMap) -> Result<(arg::OwnedFd, u16), dbus::Error>;
    fn acquire_notify(&self, options: arg::PropMap) -> Result<(arg::OwnedFd, u16), dbus::Error>;
    fn start_notify(&self) -> Result<(), dbus::Error>;
    fn stop_notify(&self) -> Result<(), dbus::Error>;
    fn uuid(&self) -> Result<String, dbus::Error>;
    fn service(&self) -> Result<dbus::Path<'static>, dbus::Error>;
    fn value(&self) -> Result<Vec<u8>, dbus::Error>;
    fn notifying(&self) -> Result<bool, dbus::Error>;
    fn flags(&self) -> Result<Vec<String>, dbus::Error>;
    fn write_acquired(&self) -> Result<bool, dbus::Error>;
    fn notify_acquired(&self) -> Result<bool, dbus::Error>;
    fn mtu(&self) -> Result<u16, dbus::Error>;
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgBluezGattCharacteristic1 for blocking::Proxy<'a, C> {

    fn read_value(&self, options: arg::PropMap) -> Result<Vec<u8>, dbus::Error> {
        self.method_call("org.bluez.GattCharacteristic1", "ReadValue", (options, ))
            .and_then(|r: (Vec<u8>, )| Ok(r.0, ))
    }

    fn write_value(&self, value: Vec<u8>, options: arg::PropMap) -> Result<(), dbus::Error> {
        self.method_call("org.bluez.GattCharacteristic1", "WriteValue", (value, options, ))
    }

    fn acquire_write(&self, options: arg::PropMap) -> Result<(arg::OwnedFd, u16), dbus::Error> {
        self.method_call("org.bluez.GattCharacteristic1", "AcquireWrite", (options, ))
    }

    fn acquire_notify(&self, options: arg::PropMap) -> Result<(arg::OwnedFd, u16), dbus::Error> {
        self.method_call("org.bluez.GattCharacteristic1", "AcquireNotify", (options, ))
    }

    fn start_notify(&self) -> Result<(), dbus::Error> {
        self.method_call("org.bluez.GattCharacteristic1", "StartNotify", ())
    }

    fn stop_notify(&self) -> Result<(), dbus::Error> {
        self.method_call("org.bluez.GattCharacteristic1", "StopNotify", ())
    }

    fn uuid(&self) -> Result<String, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.bluez.GattCharacteristic1", "UUID")
    }

    fn service(&self) -> Result<dbus::Path<'static>, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.bluez.GattCharacteristic1", "Service")
    }

    fn value(&self) -> Result<Vec<u8>, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.bluez.GattCharacteristic1", "Value")
    }

    fn notifying(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.bluez.GattCharacteristic1", "Notifying")
    }

    fn flags(&self) -> Result<Vec<String>, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.bluez.GattCharacteristic1", "Flags")
    }

    fn write_acquired(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.bluez.GattCharacteristic1", "WriteAcquired")
    }

    fn notify_acquired(&self) -> Result<bool, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.bluez.GattCharacteristic1", "NotifyAcquired")
    }

    fn mtu(&self) -> Result<u16, dbus::Error> {
        <Self as blocking::stdintf::org_freedesktop_dbus::Properties>::get(&self, "org.bluez.GattCharacteristic1", "MTU")
    }
}

pub trait OrgFreedesktopDBusProperties {
    fn get<R0: for<'b> arg::Get<'b> + 'static>(&self, interface: &str, name: &str) -> Result<R0, dbus::Error>;
    fn set<I2: arg::Arg + arg::Append>(&self, interface: &str, name: &str, value: I2) -> Result<(), dbus::Error>;
    fn get_all(&self, interface: &str) -> Result<arg::PropMap, dbus::Error>;
}

#[derive(Debug)]
pub struct OrgFreedesktopDBusPropertiesPropertiesChanged {
    pub interface: String,
    pub changed_properties: arg::PropMap,
    pub invalidated_properties: Vec<String>,
}

impl arg::AppendAll for OrgFreedesktopDBusPropertiesPropertiesChanged {
    fn append(&self, i: &mut arg::IterAppend) {
        arg::RefArg::append(&self.interface, i);
        arg::RefArg::append(&self.changed_properties, i);
        arg::RefArg::append(&self.invalidated_properties, i);
    }
}

impl arg::ReadAll for OrgFreedesktopDBusPropertiesPropertiesChanged {
    fn read(i: &mut arg::Iter) -> Result<Self, arg::TypeMismatchError> {
        Ok(OrgFreedesktopDBusPropertiesPropertiesChanged {
            interface: i.read()?,
            changed_properties: i.read()?,
            invalidated_properties: i.read()?,
        })
    }
}

impl dbus::message::SignalArgs for OrgFreedesktopDBusPropertiesPropertiesChanged {
    const NAME: &'static str = "PropertiesChanged";
    const INTERFACE: &'static str = "org.freedesktop.DBus.Properties";
}

impl<'a, T: blocking::BlockingSender, C: ::std::ops::Deref<Target=T>> OrgFreedesktopDBusProperties for blocking::Proxy<'a, C> {

    fn get<R0: for<'b> arg::Get<'b> + 'static>(&self, interface: &str, name: &str) -> Result<R0, dbus::Error> {
        self.method_call("org.freedesktop.DBus.Properties", "Get", (interface, name, ))
            .and_then(|r: (arg::Variant<R0>, )| Ok((r.0).0, ))
    }

    fn set<I2: arg::Arg + arg::Append>(&self, interface: &str, name: &str, value: I2) -> Result<(), dbus::Error> {
        self.method_call("org.freedesktop.DBus.Properties", "Set", (interface, name, arg::Variant(value), ))
    }

    fn get_all(&self, interface: &str) -> Result<arg::PropMap, dbus::Error> {
        self.method_call("org.freedesktop.DBus.Properties", "GetAll", (interface, ))
            .and_then(|r: (arg::PropMap, )| Ok(r.0, ))
    }
}
