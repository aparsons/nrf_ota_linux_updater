use std::collections::HashMap;
use std::convert::TryFrom;
use std::fs::read;
use std::thread;
use std::time::Duration;
use std::path;

use crc::Crc;
use crossbeam::channel::{unbounded, Sender, TryRecvError};
use num_enum::{IntoPrimitive, TryFromPrimitive};

use crate::dbus_comm::{Command, DBusCommand, DBusCommunicator, DBusEvent, Device, Event, ServiceData};

const MAX_NUM_CONN_ATTEMPTS: u8 = 3;
const CRC_ALG: Crc<u32> = Crc::<u32>::new(&crc::CRC_32_ISO_HDLC);

#[derive(Debug, PartialEq, IntoPrimitive, Clone, Copy)]
#[repr(u8)]
enum NrfObject {
  Command = 0x01,
  Data = 0x02,
}

#[derive(Debug, PartialEq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
enum DfuNrfCommand {
  Create = 0x01,
  SetReceiptNotification = 0x02,
  CalculateChecksum = 0x03,
  Execute = 0x04,
  Select = 0x06,
  Unknown = 0xFF,
}

#[derive(Debug, PartialEq, IntoPrimitive, TryFromPrimitive)]
#[repr(u8)]
enum NormalNrfCommand {
  EnterDfuMode = 0x01,
  SetAdvertisementName = 0x02,
}

#[derive(Debug, PartialEq, TryFromPrimitive)]
#[repr(u8)]
enum NormalNrfResponse {
  InvalidCode = 0x00,
  Success = 0x01,
  OpCodeNotSupported = 0x02,
  OperationFailed = 0x04,
  InvalidAdvertisementName = 0x05,
  Busy = 0x06,
  NotBonded = 0x07,
  ResponseCode = 0x20,
}

#[derive(Debug, PartialEq, TryFromPrimitive)]
#[repr(u8)]
enum DfuNrfResponse {
  InvalidCode = 0x00,
  Success = 0x01,
  OpCodeNotSupported = 0x02,
  InvalidParameter = 0x03,
  InsufficientResources = 0x04,
  InvalidObject = 0x05,
  UnsupportedType = 0x07,
  OperationNotPermitted = 0x08,
  OperationFailed = 0x0A,
  ExtendedError = 0x0B,
  ResponseCode = 0x60,
  Unknown = 0xFF,
}

#[derive(Debug)]
enum State {
  Disabled,
  ScanningForDevice(Device),
  ConnectingToDevice(Device),
  DisconnectingFromDevice(Device),
  WaitingForEnableNotificationResponse(Device),
  WaitingForDfuTriggerResponse,
  WaitingForSelectResponse(NrfObject),
  WaitingForCreateObjectResponse(NrfObject),
  WaitingForSetReceiptNotificationResponse,
  WaitingForCrcChecksumResponse(NrfObject),
  WaitingForExecuteResponse(NrfObject),
  Finished,
}

#[derive(Debug, PartialEq)]
enum Action {
  DoNothing,
  Start,
  ScanForDevice(Device),
  DisconnectFromDevice(Device),
  ConnectToDevice(Device),
  CheckIfFullyConnected(Device),
  CheckIfFullyDisconnected(Device),
  GetServicePaths(Device),
  EnableNotifications(Device),
  TriggerDfuMode,
  SetupPacketParameters(NrfObject, u32, u32, u32),
  CheckIfPacketMalformed(NrfObject, u32, u32),
  SendSelectCommand(NrfObject),
  SendCreateObjectCommand(NrfObject),
  SendSetReceiptNotificationCommand,
  SendDataPacket(NrfObject),
  SendCrcChecksumCommand(NrfObject),
  SendExecuteCommand(NrfObject),
  CheckIfFinished,
  FinishUpdate,
}

#[derive(Debug)]
struct CharacteristicPaths {
  dfu: String,
  ctrl_point: String,
  bulk_data: String,
}

#[derive(Debug)]
struct PacketInformation {
  init_file_size: usize,
  max_init_file_chunk_size: u32,
  fw_image_file_size: usize,
  max_fw_image_file_chunk_size: u32,
  current_fw_image_offset: u32,
  current_fw_image_chunk_size: u32,
}

#[derive(Debug)]
struct Files {
  init: Vec<u8>,
  fw_image: Vec<u8>,
}

#[derive(Debug)]
struct ActionData {
  char_paths: CharacteristicPaths,
  packet_info: PacketInformation,
  conn_attempts: u8,
  comm: DBusCommunicator,
  event_sender: Sender<DBusEvent>,
  files: Files,
  cmd_sender: Sender<DBusCommand>,
}

impl ActionData {
  pub fn new(
    init_file_size: usize,
    fw_image_file_size: usize,
    comm: DBusCommunicator,
    event_sender: Sender<DBusEvent>,
    files: Files,
    cmd_sender: Sender<DBusCommand>,
  ) -> Self {
    ActionData {
      char_paths: CharacteristicPaths {
        dfu: String::new(),
        ctrl_point: String::new(),
        bulk_data: String::new(),
      },
      packet_info: PacketInformation {
        init_file_size,
        max_init_file_chunk_size: 0,
        fw_image_file_size,
        max_fw_image_file_chunk_size: 0,
        current_fw_image_offset: 0,
        current_fw_image_chunk_size: 0,
      },
      conn_attempts: 0,
      comm,
      event_sender,
      files,
      cmd_sender,
    }
  }
}

pub struct Controller {
  mac_addr: String,
  dfu_mac_addr: String,
}

impl Controller {
  pub fn new(mac_addr: String, dfu_mac_addr: String) -> Self {
    Controller { mac_addr, dfu_mac_addr }
  }

  pub fn start(&self) {
    let (event_s, event_r) = unbounded();
    let (cmd_s, cmd_r) = unbounded();
    let mut current_state = State::Disabled;

    let comm = DBusCommunicator::new(event_s.clone(), cmd_r, &self.mac_addr, &self.dfu_mac_addr);

    let files;
    if path::Path::new("app.dat").exists() && path::Path::new("app.bin").exists() {
      files = Files {
        init: read("app.dat").unwrap(),
        fw_image: read("app.bin").unwrap(),
      };
    } else if path::Path::new("nrf_files/app.dat").exists() && path::Path::new("nrf_files/app.bin").exists() {
      files = Files {
        init: read("nrf_files/app.dat").unwrap(),
        fw_image: read("nrf_files/app.bin").unwrap(),
      };
    } else {
      panic!("Unable to find DFU files");
    }

    let mut action_data = ActionData::new(
      files.init.len(),
      files.fw_image.len(),
      comm.clone(),
      event_s.clone(),
      files,
      cmd_s,
    );

    thread::spawn(move || {
      log::debug!("Init file size: {} bytes", action_data.packet_info.init_file_size);
      log::debug!(
        "FW image file size: {} bytes",
        action_data.packet_info.fw_image_file_size
      );

      // setup the initial state of the state machine
      let mut action = Action::Start;
      while action != Action::DoNothing {
        action = Self::perform_action(action, &mut current_state, &mut action_data);
      }

      loop {
        match event_r.try_recv() {
          Ok(x) => {
            // process the event and based on the current state determine what should be done
            action = Self::process_event(&current_state, x.dev.unwrap(), &x.evt);

            // Continuously perform actions until we're in a state where we're waiting for a response
            // from the device. When one of these states is reached, DoNothing will be returned as an action
            // and the loop will exit
            while action != Action::DoNothing {
              action = Self::perform_action(action, &mut current_state, &mut action_data);
            }
          }
          Err(e) => match e {
            TryRecvError::Empty => (),
            TryRecvError::Disconnected => {
              log::error!("Status channel disconnected. Exiting");
              std::process::exit(1);
            }
          },
        }

        thread::sleep(Duration::from_millis(100));
      }
    });
  }

  fn get_characteristic_path(
    service_data: &HashMap<String, ServiceData>,
    service_uuid: &str,
    char_uuid: &str,
  ) -> Option<String> {
    for (uuid, data) in service_data.iter() {
      if uuid == service_uuid {
        for c in &data.characteristics {
          if c.uuid == char_uuid {
            return Some(c.characteristic_path.clone());
          }
        }
      }
    }

    None
  }

  fn process_event(curr_state: &State, device: Device, evt: &Event) -> Action {
    let mut action = Action::DoNothing;

    // The responsibility of this function is to merely determine the next action based on the event received and
    // the current state. No state changes should occur here

    match curr_state {
      State::ScanningForDevice(dev) => match evt {
        Event::Added => {
          action = Action::ConnectToDevice(*dev);
        }
        Event::Timeout => {
          if device == *dev {
            log::error!("Failed to find device");
            std::process::exit(1);
          }
        }
        Event::DBusError(err) => {
          log::error!("{:?}", err);
        }
        _ => {}
      },
      State::ConnectingToDevice(dev) => match evt {
        Event::Connected => {
          action = Action::CheckIfFullyConnected(*dev);
        }
        Event::ServicesResolved => {
          action = Action::CheckIfFullyConnected(*dev);
        }
        Event::DBusError(err) => {
          log::error!("{:?}", err);
          action = Action::ConnectToDevice(*dev);
        }
        _ => {}
      },
      State::DisconnectingFromDevice(dev) => match evt {
        Event::Disconnected => {
          action = Action::CheckIfFullyDisconnected(*dev);
        }
        Event::ServicesUnresolved => {
          action = Action::CheckIfFullyDisconnected(*dev);
        }
        Event::DBusError(err) => {
          log::error!("{:?}", err);
        }
        _ => {}
      },
      State::WaitingForEnableNotificationResponse(dev) => match evt {
        Event::NotificationsEnabled => match dev {
          Device::Normal => {
            action = Action::TriggerDfuMode;
          }
          Device::Dfu => {
            action = Action::SendSelectCommand(NrfObject::Command);
          }
        },
        Event::Disconnected => {
          log::warn!("Lost connection to DFU device. Attempting to reconnect");
          action = Action::ConnectToDevice(*dev);
        }
        Event::DBusError(err) => {
          log::error!("{:?}", err);
        }
        _ => {}
      },
      State::WaitingForDfuTriggerResponse => match evt {
        Event::ValueReceived(vals) => {
          if NormalNrfResponse::try_from(vals[0]).unwrap() == NormalNrfResponse::ResponseCode
            && NormalNrfCommand::try_from(vals[1]).unwrap() == NormalNrfCommand::EnterDfuMode
          {
            if NormalNrfResponse::try_from(vals[2]).unwrap() == NormalNrfResponse::Success {
              action = Action::DisconnectFromDevice(Device::Normal);
            } else {
              log::error!("Received DFU trigger response code: {}", vals[2]);
              std::process::exit(1);
            }
          }
        }
        Event::Disconnected => {
          log::warn!("Lost connection to DFU device. Attempting to reconnect");
          action = Action::ConnectToDevice(Device::Dfu);
        }
        Event::DBusError(err) => {
          log::error!("{:?}", err);
        }
        _ => {}
      },
      State::WaitingForSelectResponse(obj) => match evt {
        Event::ValueReceived(vals) => {
          if DfuNrfResponse::try_from(vals[0]).unwrap() == DfuNrfResponse::ResponseCode
            && DfuNrfCommand::try_from(vals[1]).unwrap() == DfuNrfCommand::Select
          {
            if DfuNrfResponse::try_from(vals[2]).unwrap() == DfuNrfResponse::Success {
              // max_size is padded for four bytes but only uses two bytes hence the gap here
              // between size and offset
              let max_size = ((vals[4] as u32) << 8) | (vals[3] as u32);

              let offset =
                ((vals[10] as u32) << 24) | ((vals[9] as u32) << 16) | ((vals[8] as u32) << 8) | (vals[7] as u32);

              let crc =
                ((vals[14] as u32) << 24) | ((vals[13] as u32) << 16) | ((vals[12] as u32) << 8) | (vals[11] as u32);

              action = Action::SetupPacketParameters(*obj, max_size, offset, crc);
            } else {
              log::error!("Received Select response code: {}", vals[2]);
              std::process::exit(1);
            }
          }
        }
        Event::Disconnected => {
          log::warn!("Lost connection to DFU device. Attempting to reconnect");
          action = Action::ConnectToDevice(Device::Dfu);
        }
        Event::DBusError(err) => {
          log::error!("{:?}", err);
        }
        _ => {}
      },
      State::WaitingForCreateObjectResponse(obj) => match evt {
        Event::ValueReceived(vals) => {
          if DfuNrfResponse::try_from(vals[0]).unwrap() == DfuNrfResponse::ResponseCode
            && DfuNrfCommand::try_from(vals[1]).unwrap() == DfuNrfCommand::Create
          {
            if DfuNrfResponse::try_from(vals[2]).unwrap() == DfuNrfResponse::Success {
              match obj {
                NrfObject::Command => {
                  action = Action::SendSetReceiptNotificationCommand;
                }
                NrfObject::Data => {
                  thread::sleep(Duration::from_millis(400));
                  action = Action::SendDataPacket(*obj);
                }
              }
            } else {
              log::error!("Received Select response code: {}", vals[2]);
              std::process::exit(1);
            }
          }
        }
        Event::Disconnected => {
          log::warn!("Lost connection to DFU device. Attempting to reconnect");
          action = Action::ConnectToDevice(Device::Dfu);
        }
        Event::DBusError(err) => {
          log::error!("{:?}", err);
        }
        _ => {}
      },
      State::WaitingForSetReceiptNotificationResponse => match evt {
        Event::ValueReceived(vals) => {
          if DfuNrfResponse::try_from(vals[0]).unwrap() == DfuNrfResponse::ResponseCode
            && DfuNrfCommand::try_from(vals[1]).unwrap() == DfuNrfCommand::SetReceiptNotification
          {
            if DfuNrfResponse::try_from(vals[2]).unwrap() == DfuNrfResponse::Success {
              action = Action::SendDataPacket(NrfObject::Command);
            } else {
              log::error!("Received Receipt Notification response code: {}", vals[2]);
              std::process::exit(1);
            }
          }
        }
        Event::Disconnected => {
          log::warn!("Lost connection to DFU device. Attempting to reconnect");
          action = Action::ConnectToDevice(Device::Dfu);
        }
        Event::DBusError(err) => {
          log::error!("{:?}", err);
        }
        _ => {}
      },
      State::WaitingForCrcChecksumResponse(obj) => match evt {
        Event::ValueReceived(vals) => {
          if DfuNrfResponse::try_from(vals[0]).unwrap() == DfuNrfResponse::ResponseCode
            && DfuNrfCommand::try_from(vals[1]).unwrap() == DfuNrfCommand::CalculateChecksum
          {
            if DfuNrfResponse::try_from(vals[2]).unwrap() == DfuNrfResponse::Success {
              let offset =
                ((vals[6] as u32) << 24) | ((vals[5] as u32) << 16) | ((vals[4] as u32) << 8) | (vals[3] as u32);

              let crc =
                ((vals[10] as u32) << 24) | ((vals[9] as u32) << 16) | ((vals[8] as u32) << 8) | (vals[7] as u32);

              action = Action::CheckIfPacketMalformed(*obj, offset, crc);
            } else {
              log::error!("Received CRC checksum response code: {}", vals[2]);
              std::process::exit(1);
            }
          }
        }
        Event::Disconnected => {
          log::warn!("Lost connection to DFU device. Attempting to reconnect");
          action = Action::ConnectToDevice(Device::Dfu);
        }
        Event::DBusError(err) => {
          log::error!("{:?}", err);
        }
        _ => {}
      },
      State::WaitingForExecuteResponse(obj) => match evt {
        Event::ValueReceived(vals) => {
          if DfuNrfResponse::try_from(vals[0]).unwrap() == DfuNrfResponse::ResponseCode
            && DfuNrfCommand::try_from(vals[1]).unwrap() == DfuNrfCommand::Execute
          {
            if DfuNrfResponse::try_from(vals[2]).unwrap() == DfuNrfResponse::Success {
              match obj {
                NrfObject::Command => {
                  log::info!("Init packet successfully transferred");
                  action = Action::SendSelectCommand(NrfObject::Data);
                }
                NrfObject::Data => {
                  action = Action::CheckIfFinished;
                }
              }
            } else {
              log::error!("Received Execute response code: {}", vals[2]);
              std::process::exit(1);
            }
          }
        }
        Event::Disconnected => {
          log::warn!("Lost connection to DFU device. Attempting to reconnect");
          action = Action::ConnectToDevice(Device::Dfu);
        }
        Event::DBusError(err) => {
          log::error!("{:?}", err);
        }
        _ => {}
      },
      State::Disabled => {}
      State::Finished => {}
    }

    log::debug!(
      "\nCurrent state: {:?}\nEvent: {:?}\nDevice: {:?}\n Action: {:?}",
      curr_state,
      evt,
      device,
      action
    );

    action
  }

  fn perform_action(action: Action, current_state: &mut State, data: &mut ActionData) -> Action {
    let mut next_action = Action::DoNothing;

    // The responsibility of this function is to change the current state (if necessary) and determine the next action based on
    // the action received

    match action {
      Action::Start => {
        if data.comm.is_device_cached(Device::Normal) {
          next_action = Action::ConnectToDevice(Device::Normal);
        } else {
          next_action = Action::ScanForDevice(Device::Normal);
        }
      }
      Action::ScanForDevice(dev) => {
        log::info!("Scanning for {:?}", dev);
        if !data.comm.is_scanning() {
          data
            .cmd_sender
            .send(DBusCommand {
              cmd: Command::ScanStart,
            })
            .unwrap();
        }

        *current_state = State::ScanningForDevice(dev);

        // setup timeout
        let event_s_clone = data.event_sender.clone();
        thread::spawn(move || {
          thread::sleep(Duration::from_secs(20));
          event_s_clone
            .clone()
            .send(DBusEvent {
              dev: Some(dev),
              evt: Event::Timeout,
            })
            .unwrap();
        });
      }
      Action::ConnectToDevice(dev) => {
        if data.conn_attempts < MAX_NUM_CONN_ATTEMPTS {
          if data.conn_attempts > 0 {
            data
              .cmd_sender
              .send(DBusCommand {
                cmd: Command::Disconnect(dev),
              })
              .unwrap();
          }

          data.conn_attempts += 1;
          log::info!(
            "Connecting to {:?}. Attempt [{}/{}]",
            dev,
            data.conn_attempts,
            MAX_NUM_CONN_ATTEMPTS
          );
          data
            .cmd_sender
            .send(DBusCommand {
              cmd: Command::Connect(dev),
            })
            .unwrap();

          *current_state = State::ConnectingToDevice(dev);
        } else {
          log::error!("Failed to reconnect to device");
          std::process::exit(1);
        }
      }
      Action::DisconnectFromDevice(dev) => {
        log::info!("Disconnecting from {:?}", dev);
        data
          .cmd_sender
          .send(DBusCommand {
            cmd: Command::Disconnect(dev),
          })
          .unwrap();

        *current_state = State::DisconnectingFromDevice(dev);
      }
      Action::CheckIfFullyConnected(dev) => {
        if data.comm.is_connected(dev) && data.comm.are_services_resolved(dev) {
          log::info!("Connected to {:?}", dev);
          next_action = Action::GetServicePaths(dev);
          data.conn_attempts = 0;
        }
      }
      Action::CheckIfFullyDisconnected(dev) => match dev {
        Device::Normal => {
          if !data.comm.is_connected(dev) && !data.comm.are_services_resolved(dev) {
            log::info!("Disconnected from {:?}", dev);
            if data.comm.is_device_cached(Device::Dfu) {
              next_action = Action::ConnectToDevice(Device::Dfu);
            } else {
              next_action = Action::ScanForDevice(Device::Dfu);
            }
          }
        }
        Device::Dfu => {
          log::error!("Fully disconnected from Dfu device. Invalid state");
        }
      },
      Action::GetServicePaths(dev) => {
        let service_data = data.comm.get_services(dev);
        match dev {
          Device::Normal => {
            match Self::get_characteristic_path(
              &service_data,
              "0000fe59-0000-1000-8000-00805f9b34fb",
              "8ec90003-f315-4f60-9fb8-838830daea50",
            ) {
              Some(path) => {
                data.char_paths.dfu = path;
              }
              None => {
                panic!("Couldn't find path");
              }
            }
          }
          Device::Dfu => {
            match Self::get_characteristic_path(
              &service_data,
              "0000fe59-0000-1000-8000-00805f9b34fb",
              "8ec90001-f315-4f60-9fb8-838830daea50",
            ) {
              Some(path) => {
                data.char_paths.ctrl_point = path;
              }
              None => {
                panic!("Couldn't find path");
              }
            }

            match Self::get_characteristic_path(
              &service_data,
              "0000fe59-0000-1000-8000-00805f9b34fb",
              "8ec90002-f315-4f60-9fb8-838830daea50",
            ) {
              Some(path) => {
                data.char_paths.bulk_data = path;
              }
              None => {
                panic!("Couldn't find path");
              }
            }
          }
        }

        next_action = Action::EnableNotifications(dev);
      }
      Action::EnableNotifications(dev) => {
        let path;
        match dev {
          Device::Normal => {
            path = data.char_paths.dfu.clone();
          }
          Device::Dfu => {
            path = data.char_paths.ctrl_point.clone();
          }
        }

        if data.comm.is_notifying(&path) {
          log::info!("Notifications for {:?} already enabled", dev);

          match dev {
            Device::Normal => {
              next_action = Action::TriggerDfuMode;
            }
            Device::Dfu => {
              next_action = Action::SendSelectCommand(NrfObject::Command);
            }
          }
        } else {
          log::info!("Enabling notifications for {:?}", dev);

          data
            .cmd_sender
            .send(DBusCommand {
              cmd: Command::EnableNotifications(dev, path.clone()),
            })
            .unwrap();

          *current_state = State::WaitingForEnableNotificationResponse(dev);
        }
      }
      Action::TriggerDfuMode => {
        data
          .cmd_sender
          .send(DBusCommand {
            cmd: Command::WriteValue(Device::Normal, data.char_paths.dfu.clone(), vec![0x01]),
          })
          .unwrap();

        log::info!("Triggering DFU mode");
        *current_state = State::WaitingForDfuTriggerResponse;
      }
      Action::SetupPacketParameters(obj, max_size, offset, crc) => match obj {
        NrfObject::Command => {
          let init_packet_len = u32::try_from(data.packet_info.init_file_size).unwrap();

          data.packet_info.max_init_file_chunk_size = if max_size > init_packet_len {
            init_packet_len
          } else {
            max_size
          };

          log::info!(
            "Max init packet size set to {} bytes",
            data.packet_info.max_init_file_chunk_size
          );

          next_action = Action::CheckIfPacketMalformed(obj, offset, crc);
        }
        NrfObject::Data => {
          let fw_image_len = u32::try_from(data.packet_info.fw_image_file_size).unwrap();

          data.packet_info.max_fw_image_file_chunk_size = if max_size > fw_image_len {
            fw_image_len
          } else {
            max_size
          };

          if offset != 0 && crc != 0 {
            let remaining_total_bytes = u32::try_from(data.packet_info.fw_image_file_size).unwrap() - offset;
            let bytes_successfully_sent = offset % data.packet_info.max_fw_image_file_chunk_size;
            if bytes_successfully_sent == 0 {
              // a full packet was received
              next_action = Action::CheckIfPacketMalformed(obj, offset, crc);
            } else {
              // a partial packet was received so reset back to last known good packet
              data.packet_info.current_fw_image_offset = offset - bytes_successfully_sent;
              if remaining_total_bytes > data.packet_info.max_fw_image_file_chunk_size {
                data.packet_info.current_fw_image_chunk_size = data.packet_info.max_fw_image_file_chunk_size;
              } else {
                data.packet_info.current_fw_image_chunk_size = remaining_total_bytes;
              }

              next_action = Action::SendCreateObjectCommand(obj);
            }
          } else {
            data.packet_info.current_fw_image_offset = 0;
            data.packet_info.current_fw_image_chunk_size = data.packet_info.max_fw_image_file_chunk_size;
            next_action = Action::SendCreateObjectCommand(obj);
          }

          log::info!(
            "Max fw image packet size set to {} bytes",
            data.packet_info.max_fw_image_file_chunk_size
          );
        }
      },
      Action::CheckIfPacketMalformed(obj, offset, crc) => match obj {
        NrfObject::Command => {
          let expected_crc = CRC_ALG.checksum(&data.files.init);
          if expected_crc == crc {
            log::debug!("CRC matched");
            next_action = Action::SendExecuteCommand(obj);
          } else {
            if offset != 0 && crc != 0 {
              log::warn!(
                "Expected CRC: {}, Received CRC: {}. Resending packet",
                expected_crc,
                crc
              );
            } else {
              log::info!("First transfer of init packet");
            }

            next_action = Action::SendCreateObjectCommand(obj);
          }
        }
        NrfObject::Data => {
          let expected_crc = CRC_ALG.checksum(&data.files.fw_image[0..usize::try_from(offset).unwrap()]);

          if expected_crc == crc {
            log::debug!("CRC matched");
            data.packet_info.current_fw_image_offset = offset;
            next_action = Action::SendExecuteCommand(obj);
          } else {
            log::warn!(
              "Expected CRC: {}, Received CRC: {}. Resending packet",
              expected_crc,
              crc
            );

            // determine last packet size
            let offset_remainder = offset % data.packet_info.max_fw_image_file_chunk_size;
            if offset_remainder == 0 {
              // the last packet was a max sized chunk
              data.packet_info.current_fw_image_offset = offset - data.packet_info.max_fw_image_file_chunk_size;
            } else {
              data.packet_info.current_fw_image_offset = offset - offset_remainder;
            }

            next_action = Action::SendCreateObjectCommand(obj);
          }

          let bytes_in_fw_image_left =
            u32::try_from(data.packet_info.fw_image_file_size).unwrap() - data.packet_info.current_fw_image_offset;

          data.packet_info.current_fw_image_chunk_size =
            if bytes_in_fw_image_left > data.packet_info.max_fw_image_file_chunk_size {
              data.packet_info.max_fw_image_file_chunk_size
            } else {
              bytes_in_fw_image_left
            };
        }
      },
      Action::SendSelectCommand(obj) => {
        data
          .cmd_sender
          .send(DBusCommand {
            cmd: Command::WriteValue(
              Device::Dfu,
              data.char_paths.ctrl_point.clone(),
              vec![DfuNrfCommand::Select.into(), obj.into()],
            ),
          })
          .unwrap();

        *current_state = State::WaitingForSelectResponse(obj);
      }
      Action::SendCreateObjectCommand(obj) => {
        let packet_size;
        match obj {
          NrfObject::Command => {
            packet_size = data.packet_info.max_init_file_chunk_size;
          }
          NrfObject::Data => {
            packet_size = data.packet_info.current_fw_image_chunk_size;
          }
        }

        data
          .cmd_sender
          .send(DBusCommand {
            cmd: Command::WriteValue(
              Device::Dfu,
              data.char_paths.ctrl_point.clone(),
              vec![
                DfuNrfCommand::Create.into(),
                obj.into(),
                (packet_size & 0xFF) as u8,
                ((packet_size >> 8) & 0xFF) as u8,
                ((packet_size >> 16) & 0xFF) as u8,
                ((packet_size >> 24) & 0xFF) as u8,
              ],
            ),
          })
          .unwrap();

        *current_state = State::WaitingForCreateObjectResponse(obj);
      }
      Action::SendSetReceiptNotificationCommand => {
        data
          .cmd_sender
          .send(DBusCommand {
            cmd: Command::WriteValue(
              Device::Dfu,
              data.char_paths.ctrl_point.clone(),
              vec![
                DfuNrfCommand::SetReceiptNotification.into(),
                (0 & 0xFF) as u8,
                ((0 >> 8) & 0xFF) as u8,
              ],
            ),
          })
          .unwrap();

        *current_state = State::WaitingForSetReceiptNotificationResponse;
      }
      Action::SendDataPacket(obj) => {
        match obj {
          NrfObject::Command => {
            let mut bytes_left = data.packet_info.max_init_file_chunk_size.try_into().unwrap();
            let mut start_pos: usize = 0;
            let mut bytes_to_write: usize;

            // send in 20 byte chunks
            while bytes_left > 0 {
              if bytes_left >= 20 {
                bytes_to_write = 20;
              } else {
                bytes_to_write = bytes_left;
              }

              data
                .cmd_sender
                .send(DBusCommand {
                  cmd: Command::WriteValue(
                    Device::Dfu,
                    data.char_paths.bulk_data.clone(),
                    data.files.init[start_pos..(start_pos + bytes_to_write)].into(),
                  ),
                })
                .unwrap();

              start_pos += bytes_to_write;
              bytes_left -= bytes_to_write;
            }
          }
          NrfObject::Data => {
            let mut start_pos: usize = data.packet_info.current_fw_image_offset.try_into().unwrap();
            let mut bytes_to_write: usize;

            let mut bytes_in_chunk_left = data.packet_info.current_fw_image_chunk_size;

            // send in 20 byte chunks
            while bytes_in_chunk_left > 0 {
              if bytes_in_chunk_left >= 20 {
                bytes_to_write = 20;
              } else {
                bytes_to_write = bytes_in_chunk_left.try_into().unwrap();
              }

              data
                .cmd_sender
                .send(DBusCommand {
                  cmd: Command::WriteValue(
                    Device::Dfu,
                    data.char_paths.bulk_data.clone(),
                    data.files.fw_image[start_pos..(start_pos + bytes_to_write)].into(),
                  ),
                })
                .unwrap();

              start_pos += bytes_to_write;
              bytes_in_chunk_left -= u32::try_from(bytes_to_write).unwrap();
            }

            data.packet_info.current_fw_image_offset += data.packet_info.current_fw_image_chunk_size;
          }
        }

        next_action = Action::SendCrcChecksumCommand(obj);
      }
      Action::SendCrcChecksumCommand(obj) => {
        data
          .cmd_sender
          .send(DBusCommand {
            cmd: Command::WriteValue(
              Device::Dfu,
              data.char_paths.ctrl_point.clone(),
              vec![DfuNrfCommand::CalculateChecksum.into()],
            ),
          })
          .unwrap();

        *current_state = State::WaitingForCrcChecksumResponse(obj);
      }
      Action::SendExecuteCommand(obj) => {
        data
          .cmd_sender
          .send(DBusCommand {
            cmd: Command::WriteValue(
              Device::Dfu,
              data.char_paths.ctrl_point.clone(),
              vec![DfuNrfCommand::Execute.into()],
            ),
          })
          .unwrap();

        *current_state = State::WaitingForExecuteResponse(obj);
      }
      Action::CheckIfFinished => {
        let percent_complete = (((data.packet_info.current_fw_image_offset as f32)
          / (data.packet_info.fw_image_file_size as f32))
          * 100.0) as u8;

        log::info!("{:.}% complete", percent_complete);
        if data.packet_info.current_fw_image_offset >= data.packet_info.fw_image_file_size.try_into().unwrap() {
          next_action = Action::FinishUpdate;
        } else {
          next_action = Action::SendCreateObjectCommand(NrfObject::Data);
        }
      }
      Action::FinishUpdate => {
        log::info!("Successfully updated device");
        *current_state = State::Finished;
        // exit program successfully
        std::process::exit(0);
      }
      Action::DoNothing => {}
    }

    log::debug!(
      "\nAction: {:?}\nCurrent state: {:?}\nNext Action: {:?}",
      action,
      current_state,
      next_action,
    );

    next_action
  }
}
