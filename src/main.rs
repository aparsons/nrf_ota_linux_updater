use chrono::Local;
use env_logger::fmt::Color;
use env_logger::Builder;
use log::LevelFilter;
use std::io::Write;
use std::env;

mod controller;
use controller::Controller;

mod dbus_bluez;
mod dbus_bluez_char;
mod dbus_bluez_device;
mod dbus_bluez_hci;
mod dbus_comm;

// const MAC_ADDRESS: &str = "EB:BA:71:30:C4:B2";
// const DFU_MAC_ADDRESS: &str = "EB:BA:71:30:C4:B3";

fn main() {
    let debug_arg = env::args().nth(1).expect("No debug level given").to_uppercase();

    let debug_level;
    if debug_arg.to_lowercase() == "trace" {
      debug_level = LevelFilter::Trace;
    } else if debug_arg.to_lowercase() == "debug"{
      debug_level = LevelFilter::Debug;
    } else if debug_arg.to_lowercase() == "info"{
      debug_level = LevelFilter::Info;
    } else {
      println!("Invalid log level set. Possibilities: trace, debug, info");
      std::process::exit(1);
    }

    log::info!("Setting debug level to: {}", debug_level);

    Builder::new()
        .format(|buf, record| {
            let mut level_style = buf.style();
            level_style.set_bold(true);

            match record.level() {
                log::Level::Trace => level_style.set_color(Color::Cyan),
                log::Level::Debug => level_style.set_color(Color::Cyan),
                log::Level::Info => level_style.set_color(Color::White),
                log::Level::Warn => level_style.set_color(Color::Yellow),
                log::Level::Error => level_style.set_color(Color::Red),
            };

            writeln!(
                buf,
                "[{}] [{}] [{}] - {}",
                Local::now().format("%H:%M:%S"),
                level_style.value(record.level()),
                record.target(),
                record.args()
            )
        })
        .filter(None, debug_level)
        .init();

    let normal_mac_addr = env::args().nth(2).expect("No normal mac address given").to_uppercase();
    let dfu_mac_addr = env::args().nth(3).expect("No dfu mac address given").to_uppercase();

    log::info!("Normal MAC address: {}", normal_mac_addr);
    log::info!("DFU MAC address: {}", dfu_mac_addr);

    let ctrl = Controller::new(normal_mac_addr, dfu_mac_addr);

    ctrl.start();

    loop {}
}
