# nrf_ota_updater

This project was spawned from the realization that Nordic Semiconductor only had two supported methods to perform Over-The-Air (OTA) updates for their SDKs: through a Windows PC or through smartphone applications. This project enables devices using BlueZ (most Linux distributions) to now perform this update. In addition, I added an auto-reconnect and restart feature to handle the not unlikely scenario of a Bluetooth device getting disconnected mid-update.

## Setup

### Hardware

On the nRF side, the PCA10040 was used. All of the firmware/update images have been generated using SDK15.3. Versions outside of SDK15 are not tested and therefore not guaranteed to work. Bonded updates were also not tested. Flashing the device will require installation of the nRF Command-Line-Tools found [here](https://www.nordicsemi.com/Products/Development-tools/nrf-command-line-tools/download#infotabs).

On the Linux side, development and testing was done using Fedora 36. Once stable, the application was cross-compiled to a Raspberry Pi 4 running Raspbian 10.

### Software

On the nRF side, I've provided a hex file that can be used to flash the Buttonless DFU sample application. Once you see your nRF device advertising, it might be a good sanity check to use the nRFConnect smartphone app and the `.zip` file also included in this repo to verify that the hardware can OTA update successfully.

On the Linux side, you'll need to be able to compile the program using Rust. If not installed, follow the installation instructions [here](https://www.rust-lang.org/tools/install).

## Building

### nRF Files

If you would like to try a different SDK version (or use your own application), you will first need to compile an application that has the Buttonless DFU service. Once successful, you'll need to compile a secure bootloader, generate bootloader settings, and merge everything together with the supported Softdevice for your SDK/chipset. Nordic has an excellent guide outlining all of these steps [here](https://devzone.nordicsemi.com/guides/short-range-guides/b/software-development-kit/posts/getting-started-with-nordics-secure-dfu-bootloader).

### Fedora 36

From the root directory, run `cargo build`

### Raspbian 10

As this application uses D-Bus, cross-compiling requires a few more steps. I opted to use the `cross` tool but there are several possibilities outlined in the DBus crate's repo [here](https://github.com/diwic/dbus-rs/blob/master/libdbus-sys/cross_compile.md). The `cross` option requires that `docker` is installed.

First, build the `rustcross:dbus-armhf` image:

```
docker build -t rustcross:dbus-armhf .
```

Then run the `cross_compile.sh` script at the root of the repo. The output binary will be in `target/armv7-unknown-linux-gnueabihf/debug`. Raspbian 10 also has Bluetooth access restricted for the `pi` user by default so you will need to add that permission like so:

```
sudo usermod -G bluetooth -a pi
```

Reboot the device or logout and log back in for this change to take effect.

## Usage

The application requires 3 pieces of information as shown below:

`./nrf_ota_updater [log level] [normal mac address] [dfu mac address]`

The log level can be `trace`, `debug`, or `info`. The DFU MAC address should be "1" greater than the normal MAC address (ie. normal = XX:XX:XX:XX:XX:B4, dfu = XX:XX:XX:XX:XX:B5). The `.dat` and `.bin` files are expected to either be (relative to the binary) in the `nrf_files` directory or at the same level as the script itself. Happy updating!